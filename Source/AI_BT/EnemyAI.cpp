// Fill out your copyright notice in the Description page of Project Settings.

#include "AI_BT.h"
#include "EnemyAI.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyCharacter.h"


AEnemyAI::AEnemyAI() {

	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourComp"));
}

void AEnemyAI::OnPossess(APawn* InPawn) {

	Super::OnPossess(InPawn);

	AEnemyCharacter *Char = Cast<AEnemyCharacter>(InPawn);

	//if char is not null and char->bot behaviour is not null 
	if (Char && Char->BotBehaviour) {
		//initialize blackboard
		BlackboardComp->InitializeBlackboard(*Char->BotBehaviour->BlackboardAsset);
		
		//assign the keyID
		EnemyKeyId = BlackboardComp->GetKeyID("Target");

		//now we want the behaviour tree to start doing its thing on possess
		BehaviorComp->StartTree(*Char->BotBehaviour);
	}
}