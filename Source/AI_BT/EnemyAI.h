// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class AI_BT_API AEnemyAI : public AAIController
{
	GENERATED_BODY()

	//transient means its initialized to 0
	UPROPERTY(transient)
	class UBlackboardComponent *BlackboardComp;
	
	UPROPERTY(transient)
		class UBehaviorTreeComponent *BehaviorComp;

public:

	AEnemyAI();

	virtual void OnPossess(APawn* InPawn) override;
	//this is the key that you need to set to the key in the blackboard (target)
	uint8 EnemyKeyId;
	
};
