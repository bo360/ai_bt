// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_BT.h"
#include "AI_BTService.h"

#include "EnemyCharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "EnemyAI.h"

#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyCharacter.h"
#include "AI_BTCharacter.h"
//#include <Engine.h>


UAI_BTService::UAI_BTService() {

	bCreateNodeInstance = true; //we want to put this on multiple AI so we want to create this as node instance
}

void UAI_BTService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) {
	AEnemyAI *EnemyPC = Cast<AEnemyAI>(OwnerComp.GetAIOwner());  //get the AI controller class

	//if we have a AI Player Controller then we 
	if (EnemyPC) { 
		AAI_BTCharacter *Enemy = Cast<AAI_BTCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn()); 

		//if enemy exists then assign the blackboard component 
		if (Enemy) {
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyId, Enemy);
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, "ENEMY IS HERE");
		}
	}
}