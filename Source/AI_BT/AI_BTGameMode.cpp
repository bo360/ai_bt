// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AI_BTGameMode.h"
#include "AI_BTCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAI_BTGameMode::AAI_BTGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
