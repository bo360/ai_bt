// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AI_BTGameMode.generated.h"

UCLASS(minimalapi)
class AAI_BTGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAI_BTGameMode();
};



