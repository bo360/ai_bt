// Fill out your copyright notice in the Description page of Project Settings.

#include "AI_BT.h"
#include "BTTask_MoveToPlayer.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyAI.h"
#include "AI_BTCharacter.h"
#include "EnemyCharacter.h"
//#include <BlackboardKeyType_Object.h>

EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) {

	AEnemyAI *CharPC = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

	AAI_BTCharacter *Enemy = Cast<AAI_BTCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->EnemyKeyId));

	if (Enemy) {
		CharPC->MoveToActor(Enemy, 5.f, true, true, true, 0, true);
		return EBTNodeResult::Succeeded;
	}
	//else {
	//	return EBTNodeResult::Failed;
	//}
	//
	return EBTNodeResult::Failed;
}
