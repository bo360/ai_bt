// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "AI_BTService.generated.h"

/**
 * 
 */
UCLASS()
class AI_BT_API UAI_BTService : public UBTService
{
	GENERATED_BODY()

public:
	UAI_BTService();
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;



	
};
